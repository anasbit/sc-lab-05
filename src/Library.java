import java.util.ArrayList;



public class Library {
	private String BorrowerName;
	private ArrayList<Book> booklist = new ArrayList<Book>() ;
	private ArrayList<ReferencesBook> Refbooklist = new ArrayList<ReferencesBook>() ;
	private ArrayList<Student> studentlist = new ArrayList<Student>();
	private ArrayList<Instructor> instructorlist = new ArrayList<Instructor>();

	
	public void addBook(Book book) {
		// TODO Auto-generated method stub
		booklist.add(book);

	}
	
	public void addStudent(Student student) {
		// TODO Auto-generated method stub
		studentlist.add(student);

	}
	
	public void addInstructor(Instructor instructor) {
		// TODO Auto-generated method stub
		instructorlist.add(instructor);

	}
	
	public void addRefBook(ReferencesBook refBook) {
		// TODO Auto-generated method stub
		Refbooklist.add(refBook);
	}
	
	public int bookCount() {
		// TODO Auto-generated method stub
		return booklist.size() + Refbooklist.size();
	}
	
	public void borrowBook(Student student,Book book) {
		// TODO Auto-generated method stub
		this.BorrowerName = student.getName();
		student.BorrowedBook(book.getName());
		booklist.remove(book);


	}
	
	
	public void borrowRefBook(Instructor instructor, ReferencesBook refbook) {
		// TODO Auto-generated method stub
		this.BorrowerName = instructor.getName();
		instructor.BorrowedBook(refbook.getName());
		Refbooklist.remove(refbook);

	}
	
	public String getBorrower(){
		// TODO Auto-generated method stub
		return this.BorrowerName;
	}
	
}

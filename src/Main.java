

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Library lib = new Library();
		
		Student student = new Student("Earl","Pongparit","5610404495");
		Student student2 = new Student("A","nas","5610400686");
		Instructor instructor = new Instructor("Jim","Jood","D14");

		Book bigjava = new Book("Big Java",  "Watson");
		Book ppl = new Book("PPL", "Ajarn");
		ReferencesBook ABC = new ReferencesBook("ABC",  "John");
		
		lib.addBook(bigjava);
		lib.addBook(ppl);
		lib.addRefBook(ABC);
		
		System.out.println(lib.bookCount()); //3
		lib.borrowBook(student,bigjava);
		
		System.out.println(lib.bookCount()); //2
		System.out.println(student.getBorrowedBook()); //Big Java
		System.out.println(lib.getBorrower()); //Earl Pongparit
		System.out.println(student.getName()); //Earl Pongparit
		
		lib.borrowRefBook(instructor, ABC);
		System.out.println(lib.bookCount()); //1
		System.out.println(instructor.getBorrowedBook()); //ABC
		System.out.println(lib.getBorrower()); //Jim Jood
		System.out.println(instructor.getName()); //Jim Jood
		
		lib.borrowBook(student2,ppl);
		
		System.out.println(lib.bookCount()); //0
		System.out.println(student.getBorrowedBook()); //Big Java
		System.out.println(lib.getBorrower()); //A nas
		System.out.println(student2.getName()); //A nas	
		
		
		
		
	}

}
